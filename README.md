# 关于PSI

PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。

# PSI演示

PSI的演示见：<a target="_blank" href="https://psi.butterfly.mopaasapp.com/">https://psi.butterfly.mopaasapp.com/</a>

PC端请用`360浏览器`或者是`谷歌浏览器`访问
 
移动端扫码访问![移动端扫码访问](PSI_Mobile_URL.png)

# PSI的开源协议

PSI的开源协议为GPL v3

# PSI相关项目

1. PSI使用帮助：https://gitee.com/crm8000/PSI_Help

2. PSI移动端：https://gitee.com/crm8000/PSI_Mobile

# 技术支持

如有技术方面的问题，可以提出Issue一起讨论：https://gitee.com/crm8000/PSI/issues

# 本地开发环境
参见：<a href="https://gitee.com/crm8000/PSI/tree/master/doc/06%20%E6%9C%AC%E5%9C%B0%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83">本地开发环境</a>

# 商务合作

微信/Tel: 13842851510

PSI商业计划见：<a href="https://gitee.com/crm8000/PSI/tree/master/doc/00%20%E5%95%86%E4%B8%9A%E8%AE%A1%E5%88%92">PSI 商业计划</a>

**我们不提供二次开发服务**

如果您有二次开发需求怎么解决？

到OSChina的众包平台：https://zb.oschina.net/ 去发布二次开发需求，让其他人接单，我们以微信和电话方式提供免费的技术支持。

这样做的好处是：

1. 您的二次开发需求以合理的成本解决
2. 让更多的程序员通过PSI赚到钱
3. PSI品牌价值进一步提升
